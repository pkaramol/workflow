#!/bin/bash

display_usage() {

    echo "Usage './autocommit.sh <no>':
    <no>=0 to autocommit both dspace-api-1.7.2.jar and dspace-jspui-api-1.7.2.jar
    <no>=1 to autocommit only dspace-jspui-api-1.7.2.jar
    <no>=1 to autocommit only dspace--api-1.7.2.jar.
    Script autocommuts the above files with the specific filenames
    and only if they have already been added to svn.
    Commits are done with empty messages, hence the -m '' param in the respective command"

}

# function that takes as filename, and if its svn status == 'M' proceeds
# with commiting it with empty message
check_and_commit() {

    FILEPATH="$1/$2"
    # echo "Filepath is " $FILEPATH
    SVN_CHECK="$(svn status $FILEPATH)"
    if [[ ! -z "${SVN_CHECK}" && "${SVN_CHECK}" =~ ^M* ]]
    then
        svn commit -m '' $FILEPATH && echo "Successfully commited $FILEPATH"
    else
        echo $FILEPATH "not in modified status -- skipping commit"
    fi

}

echo "Run with -h to see available options"
echo "This script creates automatic commits - Use at your own risk"
echo "-------------------------------------------------------------------------"


ARGS=( "$@" )
if [[ "${ARGS[@]}" ==  '--help' ]] || [[ "${ARGS[@]}" ==  '-h' ]]
	then
		display_usage
		exit 0
	fi


# if your deployments dir does not follow a pattern,
# fill in the following array with hardcoded dirs including the two jars,
# i.e. dspace-api-1.7.2.jar and dspace-jspui-api-1.7.2.jar
# separated by whitespaces, NOT commas
# e.g. IRREGULAR_PATHS=('/path/one' '/path/two')
IRREGULAR_PATHS=()
API_FILE='dspace-api-1.7.2.jar'
JSPUI_API_FILE='dspace-jspui-api-1.7.2.jar'

DEPLOYMENTS='/home/pkaramol/Workspace/tomcat-deployments'
REPOS=('lor' 'mext' 'oai' 'oep' 'seals' 'ugc' 'video')

if [ ${#IRREGULAR_PATHS[@]} -eq 0 ]; then
    for file in "${REPOS[@]}"
    do
        DIR="${DEPLOYMENTS}/${file}/WEB-INF/lib"
        echo "==============================================="
        case $1 in
            [0]*)
            check_and_commit $DIR $JSPUI_API_FILE
            check_and_commit $DIR $API_FILE
            ;;
            [1]*)
            check_and_commit $DIR $JSPUI_API_FILE
            ;;
            [2]*)
            check_and_commit $DIR $API_FILE
            ;;
            *)
            echo "Wrong script arguments, run the script with either -h or --help for usage information"
            exit 1
            ;;
        esac
    done
else
    for IRREGULAR_PATH in "${IRREGULAR_PATHS[@]}"
    do
        case $1 in
            [0]*)
            check_and_commit $IRREGULAR_PATH $JSPUI_API_FILE
            check_and_commit $IRREGULAR_PATH $API_FILE
            ;;
            [1]*)
            check_and_commit $IRREGULAR_PATH $JSPUI_API_FILE
            ;;
            [2]*)
            check_and_commit $IRREGULAR_PATH $API_FILE
            ;;
            *)
            echo "Wrong script arguments, run the script with either -h or --help for usage information"
            exit 1
            ;;
        esac
    done
fi
